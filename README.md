# Elite: Dangerous Long Range Router (Rust Version)

## Usage:

1. download `bodies.json` and `systemsWithCoordinates.json` from https://www.edsm.net/en/nightly-dumps/ and place them in the `dumps` folder
2. run `cargo +nightly install --path .` or `cargo +nightly install --git https://gitlab.com/Earthnuker/ed_lrr.git`
3. run `ed_lrr_pp --bodies dumps/bodies.json --systems dumps/systemsWithCoordinates.json`
    - Alternatively run `process.py` in the `dumps` folder
4. run `ed_lrr --help`



## Dependencies

- Working nightly Rust Compiler (tested with `rustc 1.37.0-nightly (5d8f59f4b 2019-06-04)`)
- ~8GB of free RAM
- Optional:
  - Python 3.7
  - Pandas
  - uJSON
